const path = require("path");

module.exports = (_, argv) => ({
  mode: argv.mode,
  target: ["web", "es5"],
  context: path.resolve(__dirname, "src"),
  resolve: {
    extensions: [".tsx", ".ts", ".mjs", ".js", ".json"],
  },
  stats: {
    children: false,
  },
  entry: {
    main: "./index.tsx",
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "statics/[name].js",
    chunkFilename: "statics/[name].chunk.js",
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: "ts-loader",
        options: {
          configFile: "tsconfig.json",
        },
      },
    ],
  },
});

import React, { ReactElement, useRef, useState } from "react";

interface AppProps {
  items: Item[];
}

interface Item {
  code: string;
  name: string;
}

const byTypeAheadInput = (item: Item, userInput?: string) =>
  item.name.toLowerCase().includes(userInput?.toLowerCase() ?? "");

const App: React.FC<AppProps> = (props: AppProps) => {
  const typeAhead = useRef<HTMLInputElement>(null);
  const [selectedItems, setSelectedItems] = useState<Item[]>([]);
  const [unselectedItems, setUnselectedItems] = useState<Item[]>(props.items);
  const [unselectedItemsTypeAhead, setUnselectedItemsTypeAhead] = useState<
    Item[]
  >(props.items);

  const handleOnItemClick = (item: Item) => {
    setSelectedItems([...selectedItems, item]);
    setUnselectedItems(
      unselectedItems.filter(
        (unselectedItem: Item) => unselectedItem.code !== item.code
      )
    );
  };

  const handleOnButtonClick = () => {
    setSelectedItems([]);
    setUnselectedItems(props.items);
  };

  const handleFilterItems = () => {
    if (typeAhead.current?.value ?? "".length > 0) {
      setUnselectedItemsTypeAhead(
        unselectedItems.filter((item: Item) =>
          byTypeAheadInput(item, typeAhead.current?.value)
        )
      );
    } else {
      setUnselectedItemsTypeAhead(unselectedItems);
    }
  };

  const renderItem = (item: Item): ReactElement => (
    <div
      className="item"
      key={item.code}
      onClick={() => handleOnItemClick(item)}
    >
      {item.name}
    </div>
  );

  return (
    <div id="itemsTable">
      <div id="menu">
        <input
          ref={typeAhead}
          type="text"
          placeholder="Type to filter"
          onInput={handleFilterItems}
        />
        <button
          type="button"
          className={selectedItems.length === 0 ? "disabled" : undefined}
          disabled={selectedItems.length === 0}
          onClick={handleOnButtonClick}
        >
          Clear all!!
        </button>
      </div>
      <div id="itemsData">
        <div className="unselected">
          {typeAhead.current?.value ?? "".length > 0
            ? unselectedItemsTypeAhead.map(renderItem)
            : unselectedItems.map(renderItem)}
        </div>
        <div className="selected">{selectedItems.map(renderItem)}</div>
      </div>
    </div>
  );
};

export default App;

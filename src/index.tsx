import ReactDOM from "react-dom";
import App from "./App";

ReactDOM.render(
  <App
    items={[
      { code: "item1", name: "Endeavour" },
      { code: "item2", name: "Enterprise" },
      { code: "item3", name: "Columbia" },
      { code: "item4", name: "Atlantis" },
    ]}
  />,
  document.getElementById("main")
);

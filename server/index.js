const express = require("express");
const path = require("path");
const port = process.argv[2] || 3002;
const app = express();

app.get("/", function (_, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});

app.get("/styles.css", function (_, res) {
  res.sendFile(path.join(__dirname + "/styles.css"));
});

app.get("/build/**", function (_, res) {
  res.sendFile(path.join(__dirname, "../build/statics", "main.js"));
});

app.listen(port, function (error) {
  if (error != null) {
    console.error(error);
    return;
  }
  console.info(`Listening at http://localhost:${port}\n`);
});
